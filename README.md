# Suchbot, a bot for suchmemes ._.

# Supported platforms:
 - [x] Mastodon (Come check it out at [@wownero](https://mastodon.social/@wownero))
 - [x] Tumblr (only posts image due to the package limitations. Check out the feed here: [wownero](https://wownero.tumblr.com/)
 - [X] VK (Here's the profile [suchmeme](https://vk.com/suchmeme)
 - [ ] XMPP
 - [ ] IRC

## It uses a [Supabase](https://supabase.com/) database instance to track already posted memes from https://suchwow.xyz/.

> Posts image data directly to services using a PostgreSQL database.

# Reproducing:
To reproduce, set up a .env with the following and install the necessary modules found in `requirements.txt`:

```
SUPABASE_URL=
SUPABASE_KEY=

Mastodon:
MAST_TOKEN=

Tumblr:
consumer_key=
consumer_secret=
oauth_token=
oauth_secret=

VK:
vk_phone=
vk_pass=
```