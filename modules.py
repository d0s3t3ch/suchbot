from mastodon import Mastodon
import pytumblr2
import vk_api
import mimetypes
import httpx
import os
from dotenv import load_dotenv

load_dotenv()


def masto(post):
    mastodon = Mastodon(
        access_token=os.environ.get("MAST_TOKEN"),
        api_base_url='https://mastodon.social'
    )
    try:
        i = httpx.get(post["image"]).content
        mime = mimetypes.guess_type(post["image"], strict=True)[0]
        media_ids = mastodon.media_post(i, mime, description=f'{post["title"]}')["id"]
        media_ids = [media_ids]
        content = f'New meme by {post["submitter"]}!\n{post["title"]} - {post["text"]}\n{post["href"]}'
        mastodon.status_post(content, media_ids=media_ids)
        print(f'Posted {post["id"]} to Mastodon @wownero')
    except Exception as e:
        print(e)



def tumblr(post):
    client = pytumblr2.TumblrRestClient(
        os.environ.get("consumer_key"),
        os.environ.get("consumer_secret"),
        os.environ.get("oauth_token"),
        os.environ.get("oauth_secret"),
    )

    client.legacy_create_photo("wownero", state="published", tags=["wownero", "such", "meme", "wow", "memes"], caption=f'New meme by {post["submitter"]}!\n{post["title"]} - {post["text"]}', link=f'{post["href"]}', source=post["image"])
    print(f'Posted meme {post["id"]} to Tumblr')

def vk(post):
    vk_session = vk_api.VkApi(os.environ.get("vk_phone"), os.environ.get("vk_pass"))
    vk_session.auth()
    vk = vk_session.get_api()
    print("VK: ", vk.wall.post(message=f'New meme by {post["submitter"]}!\n{post["title"]} - {post["text"]}', attachments={post["href"]}))